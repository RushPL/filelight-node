A simple Filelight/Baobab alternative based on Node.js platform.

Originally created by my friend RushPL. Licensed under WTFPLv2.


    yaourt -S nodejs
    npm install
    node app.js
    xdg-open http://localhost:3003/dir//tmp/

