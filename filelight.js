#!/usr/bin/env node

var fs = require('fs');

var dir = process.argv[2] || './';
var path = require('path');

var cache = {};


function statDir(dir) {
	var dirData = {
		files: fs.readdirSync(dir).map(function(file) {
			var obj = fs.lstatSync(path.join(dir, file));
			obj.filename = file;
			obj.fullpath = path.join(dir, file);
			return obj;
		}),
		totalSize: 0
	};
	dirData.files.forEach(function(f) {
		if(f.isDirectory()) {
			var newPath = path.join(dir, f.filename);

		  if(!cache[newPath])
		    cache[newPath] = statDir(newPath);
			f.dirData = cache[newPath];
			dirData.totalSize += f.dirData.totalSize;
		}
		else if(f.isFile()) dirData.totalSize += f.size;
	});

	return dirData;
}

exports.statDir = statDir;